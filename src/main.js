import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import './assets/vendors/css/normalize.css'
import "./assets/vendors/css/grid.css"
import "./assets/vendors/css/ionicons.min.css"
import "./assets/vendors/css/animate.css"
import "./assets/resources/css/style.css"
import "./assets/resources/css/queries.css"

import 'jquery-waypoints/waypoints.min'
import 'html5shiv'
import '@/assets/resources/js/script'

new Vue({
    render: h => h(App),
}).$mount('#app')
